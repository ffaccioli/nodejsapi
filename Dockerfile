#FROM node:16.13.1-alpine AS builder
#COPY . .
#RUN npm install -g pkg
#RUN pkg -t node16-linux -o app .
#RUN echo %cd%


#FROM node:16.13.1-alpine
#COPY --from=builder ./app .
#RUN echo %cd%
#RUN ls -a
#EXPOSE 8443
#ENTRYPOINT ["./app"]

FROM node:16.13.1-alpine
COPY . .
RUN npm install
EXPOSE 8443
CMD ["npm", "start"]
