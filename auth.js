const jwt = require('jsonwebtoken');
const fs = require('fs');


const verifyToken = async (req, res, next) => {
   
   const token = req.headers['x-access-token'] || req.cookies['x-access-token'];
   console.log(token);
   if(!token) res.status(401).send('Unauthenticated request');

   const privateKey = fs.promises.readFile(__dirname + '/private.key', 'utf8', (err, data) => {
         if (err) return console.error(err);
         return data;
      }
   );
   
   privateKey
      .then(privateKey => {
         
         const verified = new Promise((resolve, reject) => {
            try {
               jwt.verify(token, privateKey, (err, decoded) => {
                  if(err) reject(err);
                  resolve(decoded);
                  });
            } catch (err) {
               reject(err);
            }
         });
         verified.then(bool => { next()})
                 .catch(err => {
                    console.error(err);
                    return res.status(401).send('Authentication failed');
                 });
         
      })
      .catch(err => {
         console.error(err);
         return res.status(500).send('Failed to extract jwtSecret');
      });       
           
}

module.exports = verifyToken;










