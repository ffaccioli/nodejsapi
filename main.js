/**
  BACKEND NODE JS
**/




/**
  import e loading di un modulo
  la variabile conterrà l'istanza
**/
//const https = require('https');
const http = require('http');

var express = require('express');
var app = express();

const fs = require('fs');
require("dotenv").config();
const Sequelize = require('sequelize');
const path = require('path');
const ejs = require('ejs');
const db = require('./db.js');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const mysqldep = require('mysql2');
//var UserModel = require(path.join(__dirname + '/model/user.js'));
const auth = require('./auth.js');
//const eurekaHelper = require('./eureka-helper.js');

// definizione della variabile del connettore mysql
//var mysql = require('mysql');


/**
  definizione delle variabili per SSL

const options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};
**/
app.use(express.urlencoded());
app.use(cookieParser());
app.use(express.json());

//app.engine('html', require('ejs').renderFile);
app.set('views', "./");
app.set('view engine', 'ejs');
//app.use(express.json());

/*async function populateDB(){
   const email = "fra.faccio.96@gmail.com";
   const account = await db.account.findOne({ where: { email: email } });
   if(!account){
      var encryptedPwd = await bcrypt.hash("password", 10);
      const newaccount = await db.account.build({ firstName: "Francesco", lastName: "Faccioli", password: encryptedPwd, email: email })
      console.log(newaccount);
      newaccount.save(newaccount);
   } else console.log("GIA' POPOLATO");
}*/

/*app.get('/home', async function (req, res) {   
   res.status(200).sendFile("/static-html-page.html");
})*/


/*app.get('/testing', auth, async (req, res) => {   
      return res.status(200).render('welcome');
})*/


/*app.get('/welcome', async (req, res) => {   
      res.status(200).render('welcome');
})*/


/*app.get('/login', async function (req, res){
   res.status(200).sendFile("/login.html");
})*/


app.post('/api/auth/login', async (req, res) => {
   try{
      //console.log(req);
      console.log(req.body);
      const { username, password } = req.body;
      console.log(username);
      console.log(password);
      //const credentials = JSON.parse(req.body);
      //console.log(credentials);
      if(!(username||password)) res.status(400).send("All input is required.");
      else {
         const account = await db.account.findOne({ where: { email: username  } });
         console.log(account);
         if (account && (await bcrypt.compare(password, account.password))) {
         const privateKey = await fs.promises.readFile('/etc/node-volume/private.key', 'utf8', function (err, data) {
            if (err) {
               return console.error(err);
            }
            return data;
         })
      
         const getToken = new Promise((resolve, reject) => {
            try{
               const token = jwt.sign( { user_id: account.id, username }, privateKey, { expiresIn: '10h'}, (err, token) => {
                  if (err) console.error(err);
                  console.log(token);
                  resolve(token);
               });
            } catch (err) {
               reject(err);
            }
         })

      getToken
      .then(token => {
                     res.status(200).send(token);
                     })
      .catch(err  => { 
                     console.error(err);
                     res.status(500).send("Unable to sign and generate token.");
                     });
                     
                     
      }
      else res.status(401).send("Invalid credentials");
      }
      } catch (err) {
         console.error(err);
         res.status(500).send("Internal Server Error");
      }
})


/*app.post('/login', async function (req, res){

   try{
      const { email, password } = req.body;
      console.log(email);
      console.log(password);
      if(!(email && password)) {
         res.status(400).send("All input is required")
      }
      const account = await db.account.findOne({ where: { email: email } });
      //console.log(user)
      if (account && (await bcrypt.compare(password, account.password))) {
      const privateKey = await fs.promises.readFile('/private.key', 'utf8', function (err, data) {
         if (err) {
            return console.error(err);
         }
         return data;
      })
      //console.log(privateKey);
      // Create token
      /*const token = jwt.sign( { user_id: user.id, email }, privateKey, { expiresIn: '10h'}, function(err, token){
         if (err) return console.error(err);
         console.log(token);
      });*/
      
      /*const getToken = new Promise((resolve, reject) => {
         try{
            const token = jwt.sign( { account_id: account.id, email }, privateKey, { expiresIn: '10h'}, (err, token) => {
               if (err) console.error(err);
               console.log(token);
               resolve(token);
            });
         } catch (err) {
            reject(err);
         }
      })

      getToken/*.then(token => { res.setHeader('Auth-token', token)} )
      .then(ok => { res.status(200).sendFile(path.join(__dirname + "/welcome.html"))} )*/
      /*.then(token => {
                     res.cookie('x-access-token', token, { httpOnly: true });
                     res.set('x-access-token', token);
                     res.redirect('/welcome');
                     })
      .catch(err => { console.error(err)} );

      //user.token = token;
      /*res.setHeader('Auth-token', token);
      res.status(200).sendFile(path.join(__dirname + "/welcome.html"));*/
    /*}
    else res.status(401).send("Invalid Credentials");
   } catch(err) {
      console.log(err);
      console.error("errore");
      res.status(500).json({ "error": "No user found"});
   }
   //res.status(200).sendFile(path.join(__dirname + "/welcome.html"));
});

/*app.post('addUser', function(req, res) {
   fs.readFile(__dirname + "/" + "static-json-ui.txt", "utf-8", function (err, data) {
   data = stringify(data);
   var user1 = req.params.firstname + req.params.lastname;
   data = data.append(user1);
   res.send()
   })
})*/
   


//var server = https.createServer(options, app);
var server = http.createServer(app);
const port = process.env.SERVER_PORT;

server.listen(port, async function () {
 
   //var port = server.address().port;
   await db.initDB();
   //populateDB();   
   console.log("Example app listening at http://nodejs:", port);
   
})

//eurekaHelper.registerWithEureka('nodejs', 8444);


/*https.createServer(options, function (req, res) {
  res.writeHead(200), {'Content-Type': 'text/plain'};
  res.end("hello world\n");
}).listen(8443);*/














