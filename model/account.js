const { DataTypes } = require('sequelize');

const accountModel = (sequelize, Sequelize) => {
const account = sequelize.define('account', {
	id: { type: DataTypes.BIGINT, primaryKey: true, allowNull: false, autoIncrement: true},
	firstname: { type: DataTypes.STRING },
	lastname: { type: DataTypes.STRING, notEmpty: true, allowNull: false },
	password: { type: DataTypes.STRING, notEmpty: true, allowNull: false },
	email: { type: DataTypes.STRING, unique: true, notEmpty: true },
        telephonenumber: { type: DataTypes.INTEGER }
	},
	{
         tableName: 'account',
         timestamps: false,
         createdAt: false,
         updatedAt: false
        })
	return account
}

module.exports = accountModel
