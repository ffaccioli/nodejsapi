# Progetto Microservizi

# Membri

- Francesco Faccioli (830190)
- Gusmara Andrea (831141)


# Introduzione

Quosto progetto si pone come obbiettivo la creazione di una applicazione cloud-native utilizzabile per la gestione dell'informazione e e della pubblicità con il pubblico.

# Applicazione

L'applicativo in questione ha il compito la gestione del log-in dell'utenza al sistema , che avviene dell'applicativo UX home-page tramite chiamata API.
Dopo la convalida delle credenziali con un record nel DB viene creato un token che inviato in risposta alla chiamata API viene utilizzata dagli altri servizi per la comunicazione con le API. 
L'applicativo web è sviluppo grazie all'ausilio del framework [Express](https://www.npmjs.com/package/express)  per la creazione di app nodejs intriseche con il web. 
Il microservizio utilizza l'Obeject Relational Mapper [Sequelize](https://www.npmjs.com/package/sequelize) per la connessione al DB Mysql.

 


# Cloud-native application
La nostra applicazione presenterà le caratteristiche cloud-native.
Quindi utilizzeremo il processo di sviluppo DevOps , con stage di continuos Integration e continuos Deployment (CI/CD) . Il deploy sarà erogato su macchina Azure e contenerizzata tramite Docker. 
L'applicativo contenuto in questo repository contiene uno dei 4 microservizi utilizzati per comporre il nostro sistema. 


# Stages
Di seguito vengono elencate le fasi da implementare necessarie allo svolgimento dell'assignment:

- **Build**
- **Verify**
- **Unit-test**
- **Integration-test**
- **Package**
- **Release**
- **Deploy**


