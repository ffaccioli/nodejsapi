const Sequelize = require('sequelize');
const path = require('path');

/*var db = {
   initDB: async function initDB(){
      establishConnection(1,1000);
   }
};*/

var db = {
   initDB: async function initDB(){
      initDBConnection();
      initTables();
   }
};


//connessione ad un database mysql
/*var con = mysql.createConnection({
  host: "localhost",
  user: "francesco",
  password: "Nodejs12?"
});*/

/*const sequelize = new Sequelize('gestione_account', 'user', 'Password12?', {
  host: 'localhost',
  logging: console.log,
  dialect: 'mysql' 
  // one of 'mysql' | 'mariadb' | 'postgres' | 'mssql'
});*/


const sequelize = new Sequelize('mysql://user:password@mysql-account:4307/gestione_account', {
  pool: { maxConnections: 5, maxIdleTime: 30 }
});


db.sequelize = sequelize;
db.Sequelize = Sequelize;


db.account = require("./model/account.js")(sequelize, Sequelize);


/*const connected = new Promise((resolve, reject) => {
   
      sequelize
      .authenticate().then( () => {
                             console.log("connected");
                             resolve();
                          })
             .catch( err => {
                               console.error(err);
                               //reject(err);
                            });

});

async function establishConnection(counter, timeout) {
   
   console.log("Tentativo " + counter + "...");
   connected.then( () => console.log("Connection has been established"))
            .catch( err => {
                              counter = counter + 1;
                              if (timeout > 10000) timeout = 10000;
                              else timeout = timeout * 2;
                              console.error("Failed. Retrying in " + timeout);
                              if (counter <= 9) {
                                 //setTimeout(establishConnection(counter, timeout), timeout);
                                 setTimeout( () => { establishConnection(counter, timeout)}, timeout);
                              }                              
                           })
   
}*/


async function initDBConnection(){
	try {
		await sequelize.authenticate();
		console.log('Connection has been established successfully.');
	} catch (error) {
		console.error('Unable to connect to the database:', error);
	}
}

async function initTables(){
   try {
      await sequelize.sync({});
      console.log("All models were synchronized successfully.");
   } catch (error) {
      console.error(error);
   }
}

module.exports = db;










